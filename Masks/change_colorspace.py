import cv2
import os
import re
import numpy as np

pattern = re.compile('frame[0-9]{4}')

# GOAL: turn all non-(255, 0, 0) pixels black 
for p, _, f in os.walk('.'):
   for each in f:
       if pattern.search(each):
            # each = 'frame0514_r.jpg'
            img = cv2.imread(each, cv2.IMREAD_COLOR)

            if img is None:
                continue

            h, w, c = img.shape
            if h<=0 or w<=0:
                continue

            print(each)
            cv2.imshow('test', img)
            cv2.waitKey()

            hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
            hsv_channels = cv2.split(hsv)

            # in hsv, red color wraps around h
            lower_red1 = np.array([0, 200, 200])
            upper_red1 = np.array([10, 255, 255])

            mask1 = cv2.inRange(hsv, lower_red1, upper_red1)

            lower_red2 = np.array([170, 200, 200])
            upper_red2 = np.array([180, 255, 255])

            mask2 = cv2.inRange(hsv, lower_red2, upper_red2)

            mask = mask1 | mask2

            img_w_background = cv2.bitwise_and(img, img, mask=mask)

            # Uncomment this to view single result
            # cv2.namedWindow(each, cv2.WINDOW_NORMAL)
            # cv2.resizeWindow(each, 1200, 1200)
            # cv2.imshow(each, img_w_background)

            # cv2.waitKey()
            # break

            cv2.imwrite(each, img_w_background)
exit()
